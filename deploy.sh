# 安装依赖
cd /root/zuo-config-server;
npm install --production;

# 使用 pm2 重启服务
pm2 delete config.zuo11.com;
pm2 start src/index.js -n 'config.zuo11.com';